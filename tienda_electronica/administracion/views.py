from django.shortcuts import render
# from django.contrib.auth.models import User
from .models import Tienda, Producto, Vendedor, Oferta
from .forms import TiendaForm, ProductoForm, OfertaForm, VendedorForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your views here.


def index(request):
    return render(request, 'administracion/index.html', {})


def crearTienda(request):
    if request.method == 'GET':
        tienda_form = TiendaForm()
        context = {
            'tienda_form': tienda_form
        }
        return render(request, 'administracion/crearTienda.html', context)

    elif request.method == 'POST':
        tienda_form = TiendaForm(data=request.POST)
        if tienda_form.is_valid():
            tienda_form.save()
            tienda = Tienda.objects.all()
            context = {'tienda': tienda}
            return render(request, 'administracion/tiendas.html', context)

        else:
            message = tienda_form.errors
        contexto = {
            'tienda_form': tienda_form,
        }
        return render(request, 'administracion/index.html', contexto)


def crearProducto(request):
    if request.method == 'GET':
        producto_form = ProductoForm()
        context = {
            'producto_form': producto_form
        }
        return render(request, 'administracion/crearProducto.html', context)

    elif request.method == 'POST':
        message = ''
        producto_form = ProductoForm(data=request.POST)
        if producto_form.is_valid():
            producto_form.save()
            producto = Producto.objects.all()
            context = {'producto': producto}
            return render(request, 'administracion/productos.html', context)
        else:
            message: producto_form.errors
            context = {
                'message': message,
                'producto_form': producto_form
            }
            return render(request, 'administracion/crearProducto.html', context)


def crearVendedor(request):
    if request.method == 'GET':
        vendedor_form = VendedorForm()
        context = {
            'vendedor_form': vendedor_form
        }
        return render(request, 'administracion/crearVendedor.html', context)

    elif request.method == 'POST':
        vendedor_form = VendedorForm(data=request.POST)
        if vendedor_form.is_valid():
            vendedor_form.save()
            vendedor = Vendedor.objects.all()
            context = {'vendedor': vendedor}
            return render(request, 'administracion/vendedores.html', context)
        else:
            message = vendedor_form.errors

        contexto = {
            'tienda_form': vendedor_form,
        }
        return render(request, 'administracion/vendedores.html', contexto)


def crearOferta(request):
    if request.method == 'GET':
        oferta_form = OfertaForm()
        context = {
            'oferta_form': oferta_form
        }
        return render(request, 'administracion/crearOferta.html', context)

    elif request.method == 'POST':
        message = ''
        oferta_form = OfertaForm(data=request.POST)
        if oferta_form.is_valid():
            oferta_form.save()
            oferta = Oferta.objects.all()
            context = {'oferta': oferta}
            return render(request, 'administracion/ofertas.html', context)
        else:
            message = oferta_form.errors

        contexto = {
            'tienda_form': oferta_form,
        }
        return render(request, 'administracion/ofertas.html', contexto)


def tiendas(request):
    if request.method == 'GET':
        tienda = Tienda.objects.all()
        context = {'tienda': tienda}
        return render(request, 'administracion/tiendas.html', context)


def vendedores(request):
    if request.method == 'GET':
        vendedor = Vendedor.objects.all()
        context = {'vendedor': vendedor}
        return render(request, 'administracion/vendedores.html', context)


def productos(request):
    if request.method == 'GET':
        producto = Producto.objects.all()
        context = {'producto': producto}
        return render(request, 'administracion/productos.html', context)


def ofertas(request):
    oferta = []
    if request.method == 'GET':
        if request.user.administrador:
            oferta = Oferta.objects.all()
        else:
            producto = Producto.objects.filter(tienda=request.user.tienda)
            for p in producto:
                if Oferta.objects.filter(producto_id=p.id):
                    for f in (Oferta.objects.filter(producto_id=p.id)):
                        oferta.append(f)
        context = {'oferta': oferta}
        return render(request, 'administracion/ofertas.html', context)
