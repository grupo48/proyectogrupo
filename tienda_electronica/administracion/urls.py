from django.contrib import admin
from django.urls import path
from . import views

app_name='administracion'

urlpatterns = [
    path('', views.index, name='index'),

    path('tiendas/', views.tiendas, name='tiendas'),
    path('crearTienda/', views.crearTienda, name='crearTienda'),
    path('productos/', views.productos, name='productos'),
    path('crearProducto/', views.crearProducto, name='crearProducto'),
    path('vendedores/', views.vendedores, name='vendedores'),
    path('crearVendedor/', views.crearVendedor, name='crearVendedor'),
    path('ofertas/', views.ofertas, name='ofertas'),
    path('crearOferta/', views.crearOferta, name='crearOferta'),
    
]
