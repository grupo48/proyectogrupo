from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from .models import Tienda, Oferta, Producto, Vendedor
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class TiendaForm(forms.ModelForm):
    class Meta():
        model = Tienda
        fields = ('nombre_tienda', 'direccion', 'comuna', 'telefono', 'correo_electronico',)
       

    def __init__(self, *args, **kwargs):
        super(TiendaForm, self).__init__(*args,**kwargs)
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        

class ProductoForm(forms.ModelForm):
    class Meta():
        model = Producto
        fields = ('nombre_producto', 'descripcion', 'precio', 'tipo', 'tienda', )
       

    def __init__(self, *args, **kwargs):
        super(ProductoForm, self).__init__(*args,**kwargs)
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        

class OfertaForm(forms.ModelForm):
    class Meta():
        model = Oferta
        fields = ('nombre_oferta', 'producto', 'porc_descuento', )
       

    def __init__(self, *args, **kwargs):
        super(OfertaForm, self).__init__(*args,**kwargs)
        
         
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        

class VendedorForm(UserCreationForm):
    class Meta:
        model = Vendedor
        fields = ('username', 'email', 'tienda', 'password1', 'password2', 'administrador' )

    def __init__(self, *args, **kwargs):
        super(VendedorForm, self).__init__(*args,**kwargs)
        
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
        

