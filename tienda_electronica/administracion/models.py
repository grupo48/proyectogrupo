from django.db import models
from django.contrib.auth.models import AbstractUser

class Tienda(models.Model):
    CERRILLOS = 'Cerrillos'
    SANTIAGO = 'Santiago'

    COMUNA_CHOICES = (
        (CERRILLOS, 'cerrillos'),
        (SANTIAGO, 'Santiago')
    )
    nombre_tienda = models.CharField(max_length=200)
    direccion = models.CharField(max_length=200, default="")
    comuna = models.CharField(max_length=200, choices=COMUNA_CHOICES)
    telefono = models.IntegerField(default="")
    correo_electronico = models.CharField(max_length=200, default="")

    def __str__(self):
        return self.nombre_tienda


class Vendedor(AbstractUser):
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE, null=True)
    administrador = models.BooleanField(default=False)

class Producto(models.Model):

    ABARROTES = 'Abarrotes'
    LIMPIEZA = 'Limpieza'
    VERDURAS = 'Verduras'

    TIPO_CHOICES = (
        (ABARROTES, 'Abarrotes'),
        (LIMPIEZA, 'Limpieza'),
        (VERDURAS, 'Verduras')
    )
    nombre_producto = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200, default="")
    precio = models.IntegerField(null=True)
    tipo = models.CharField(max_length=200, choices=TIPO_CHOICES, default="")
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE, default="")

    def __str__(self):
        return self.nombre_producto

        
class Oferta(models.Model):
    nombre_oferta = models.CharField(max_length=200)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True)
    porc_descuento = models.IntegerField(null=True)

    def __str__(self):
        return self.nombre_oferta
