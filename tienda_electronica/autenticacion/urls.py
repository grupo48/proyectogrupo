from django.urls import path
from .views import *

app_name = 'autenticacion'

urlpatterns = [

    # path('', include('django.contrib.auth.urls'))
    path('logout/', logout, name='logout'),
    path('login/', login, name='login'),
]
