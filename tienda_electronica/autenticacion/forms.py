from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from administracion.models import Vendedor

class loginForm(AuthenticationForm):
    class Meta:
        model=Vendedor
        fields=('username','password1')

    def __init__(self, *args, **kwargs):
        super(loginForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
