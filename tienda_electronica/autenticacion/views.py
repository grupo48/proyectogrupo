from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login as d_login, logout as d_logout
from django.contrib.auth.decorators import login_required
# from autenticacion import Vendedor
from .forms import loginForm


def home(request):
    return render(request, "autenticacion/home.html", {})

def login(request):
    form = AuthenticationForm
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            print(username)
            print(password)
            user = authenticate(username=username, password=password)
            if user is not None:
                d_login(request, user)
                return redirect('administracion:index')
            else:
                contexto = {'form': form}
                return render(request, 'login.html', contexto)
        else:
            errors = form.errors
            contexto = {
                'form': form,
                'errors': errors
                }
            return render(request, 'login.html', contexto)
    else:
        if request.user.is_authenticated:
            return redirect('administracion:index')
        else:
            contexto = {'form': form}
            return render(request, 'login.html', contexto)

def logout(request):
    d_logout(request)
    return redirect('home')
