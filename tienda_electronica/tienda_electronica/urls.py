from django.contrib import admin
from django.urls import path, include
from autenticacion import views

urlpatterns = [

    path('', views.home, name="home"),
    path('admin/', admin.site.urls),
    path('', include('autenticacion.urls')),

    # path('admin/', admin.site.urls),
    path('administracion/', include('administracion.urls')),
    path('tienda/', include('tienda.urls')),

]
