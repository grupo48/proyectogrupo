from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from administracion.models import *

# Create your models here.
class Venta(models.Model):
    vendedor = models.ForeignKey('administracion.vendedor', on_delete=models.CASCADE)
    fecha_venta =models.DateTimeField(default=timezone.now)
    subtotal = models.IntegerField(default=0)
    iva = models.IntegerField(default=0)
    total = models.IntegerField(default=0)

    def __str__(self):
        return self.vendedor.username

class DetalleVenta(models.Model):
    venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    precio = models.IntegerField(default=0)
    cantidad = models.IntegerField(default=0)
    subtotal = models.IntegerField(default=0)

    def __str__(self):
        return self.producto.nombre_producto