from django.contrib import admin
from django.urls import path
from . import views

app_name='tienda'

urlpatterns = [
    path('', views.index, name='index'),

    path('ventas/', views.ventas, name='ventas'),
    path('crearVenta/', views.crearVenta, name='crearVenta'),
    
]