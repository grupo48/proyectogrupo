from django.forms import *
from django.utils import timezone
from .models import Venta

class VentaForm(ModelForm):
    class Meta():
        model = Venta
        fields = ('fecha_venta', 'iva', 'subtotal', 'total',)
        widgets = {
            'fecha_venta': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': timezone.now().strftime('%Y-%m-%d')
                }
            ),
            'iva': TextInput(attrs={
                'class': 'form-control',
            }),
            'subtotal': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            }),
            'total': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            })
        }

    def __init__(self, *args, **kwargs):
        super(VentaForm, self).__init__(*args,**kwargs)
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})