from django.shortcuts import render

from .models import Venta
from .forms import VentaForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your views here.
def index(request):
    return render(request, 'tienda/index.html', {})


def crearVenta(request):
    if request.method == 'GET':
        venta_form = VentaForm()
        context = {
            'venta_form': venta_form
        }
        return render(request, 'tienda/crearVenta.html', context)

    elif request.method == 'POST':
        venta_form = VentaForm(data=request.POST)
        if venta_form.is_valid():
            venta_form.save()
            venta = Venta.objects.all()
            context = {'venta': venta}
            return render(request, 'tienda/ventas.html', context)

        else:
            message = venta_form.errors
        contexto = {
            'venta_form': venta_form,
        }
        return render(request, 'tienda/index.html', contexto)

def ventas(request):
    if request.method == 'GET':
        venta = Venta.objects.all()
        context = {'venta': venta}
        return render(request, 'tienda/ventas.html', context)